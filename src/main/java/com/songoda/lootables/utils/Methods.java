package com.songoda.lootables.utils;

import com.songoda.lootables.utils.version.NMSUtil;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

public class Methods {

    private static Class<?> clazzEnchantmentManager;

    private static Method methodAsBukkitCopy, methodAsNMSCopy, methodA;

    public static ItemStack applyRandomEnchants(ItemStack item, int level) {
        if (clazzEnchantmentManager == null) {
            try {
                clazzEnchantmentManager = NMSUtil.getNMSClass("EnchantmentManager");
                Class<?> clazzItemStack = NMSUtil.getNMSClass("ItemStack");
                Class<?> clazzCraftItemStack = NMSUtil.getCraftClass("inventory.CraftItemStack");

                methodAsBukkitCopy = clazzCraftItemStack.getMethod("asBukkitCopy", clazzItemStack);
                methodAsNMSCopy = clazzCraftItemStack.getMethod("asNMSCopy", ItemStack.class);

                if (NMSUtil.getVersionNumber() == 8)
                    methodA = clazzEnchantmentManager.getMethod("a", Random.class, clazzItemStack, int.class);
                else
                    methodA = clazzEnchantmentManager.getMethod("a", Random.class, clazzItemStack, int.class, boolean.class);

            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        try {
            Object nmsItemStack = methodAsNMSCopy.invoke(null, item);

            if (NMSUtil.getVersionNumber() == 8)
                nmsItemStack = methodA.invoke(null, new Random(), nmsItemStack, level);
            else
                nmsItemStack = methodA.invoke(null, new Random(), nmsItemStack, level, false);

            item = (ItemStack) methodAsBukkitCopy.invoke(null, nmsItemStack);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return item;
    }

    public static String formatText(String text) {
        if (text == null || text.equals(""))
            return "";
        return formatText(text, false);
    }

    public static String formatText(String text, boolean cap) {
        if (text == null || text.equals(""))
            return "";
        if (cap)
            text = text.substring(0, 1).toUpperCase() + text.substring(1);
        return ChatColor.translateAlternateColorCodes('&', text);
    }

    public static class Tuple<key, value> {
        public final key x;
        public final value y;

        public Tuple(key x, value y) {
            this.x = x;
            this.y = y;
        }

        public key getKey() {
            return this.x;
        }

        public value getValue() {
            return this.y;
        }
    }
}
